# Maintainer: davedatum <trom@davedatum.com>

pkgname=trom-cloud
pkgver=1.4
pkgrel=1
pkgdesc="Home of TROM (electron)"
arch=("any")
url="https://www.cloud.tromsite.com"
license=("MIT")
depends=("gtk3" "libxss" "nss")
makedepends=("imagemagick" "nodejs-nativefier")
source=(
  "${pkgname}.png"
  "${pkgname}.desktop"
  "${pkgname}-install.hook")
sha256sums=('2b6708af3dd1e81fa2287ccc53cc33fe024cd3b4ad430f4378cbdf7c50bca4cf'
            '58c18917d567a917afe4a0df1c7131a892c7a18f5cbf9b22a5846e3deee6c602'
            '1501552c75edad153b921ec5aaed7ea4516e5565fc3004ce5cd3931dd3763cdc')

build() {
  cd "${srcdir}"
  
  nativefier \
    --name "TROM-Cloud" \
    --icon "${pkgname}.png" \
    --width "800px" \
    --height "600px" \
    --verbose \
    --single-instance \
    --tray \
    "https://www.cloud.tromsite.com"
}

package() {
  install -dm755 "${pkgdir}/"{opt,usr/{bin,share/{applications,licenses/${pkgname}}}}

  cp -rL "${srcdir}/trom-cloud-linux-"* "${pkgdir}/opt/${pkgname}"
  ln -s "/opt/${pkgname}/trom-cloud" "${pkgdir}/usr/bin/${pkgname}"
  install -Dm644 "${srcdir}/${pkgname}.desktop" "${pkgdir}/usr/share/applications/${pkgname}.desktop"
  for _size in "192x192" "128x128" "96x96" "64x64" "48x48" "32x32" "24x24" "22x22" "20x20" "16x16" "8x8"
  do
    install -dm755 "${pkgdir}/usr/share/icons/hicolor/${_size}/apps"
    convert "${srcdir}/${pkgname}.png" -resize "${_size}" "${pkgdir}/usr/share/icons/hicolor/${_size}/apps/${pkgname}.png"
  done

  install -Dm644 "${srcdir}/trom-cloud-install.hook" "${pkgdir}/usr/share/libalpm/hooks/trom-cloud-install.hook"
}
